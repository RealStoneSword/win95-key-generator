#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
void usage() {
    puts(
            "Usage: win95-key-generator -o [AMOUNT]\n"
            "       win95-key-generator -c [AMOUNT]\n"
            "\n"
            "  -h, --help      Shows help\n"
            "  -o, --oem       Generates an OEM key\n"
            "  -c, --cd        Generates a CD key\n"
        );
}
char* oem() { // arg2 is for passing in the amount of keys to make
    int chunk1, chunk2, chunk3[6], chunk4, sum;
    char* key;
    key = malloc(25);
    chunk1 = (rand() % (366 - 1 +1)) + 1;
    chunk2 = (rand() % (2003 - 1995 +1)) + 1995;
    if (chunk2 < 2000)
        chunk2 = chunk2 - 1900;
    else
        chunk2 = chunk2 - 2000;
    do {
        sum = 0;
        for (int i = 0; i < 5; i++) {
            chunk3[i] = (rand() % (9 - 1 +1)) + 1;
            sum = sum + chunk3[i];
        }
        chunk3[5] = (rand() % (8 - 1 +1)) + 1;
        sum = chunk3[5] + sum;
    }while( sum % 7 != 0 );
    chunk4 = (rand() % (99999 - 1 +1)) + 1;
    sprintf(key, "%03d%02d-OEM-0%d%d%d%d%d%d-%05d", chunk1, chunk2, chunk3[0], chunk3[1], chunk3[2], chunk3[3], chunk3[4], chunk3[5], chunk4);
    return key;
}

char* cd() {
    int chunk1, chunk2[7], sum;
    char* key;
    key = malloc(15);
    chunk1 = (rand() % (999 - 1 +1)) + 1;
    while (chunk1 == 333 || chunk1 == 444 || chunk1 == 555 || chunk1 == 666 || chunk1 == 777 || chunk1 == 888 || chunk1 == 999) 
        chunk1 = (rand() % (999 - 1 +1)) + 1;
    do {
        sum = 0;
        for (int i = 0; i < 6; i++) {
            chunk2[i] = (rand() % (9 - 1 +1)) + 1;
            sum = sum + chunk2[i];
        }
        chunk2[6] = (rand() % (7 - 1 +1)) + 1;
        sum = chunk2[6] + sum;
    }while( sum % 7 != 0 );
    sprintf(key, "%03d-%d%d%d%d%d%d%d", chunk1, chunk2[0], chunk2[1], chunk2[2], chunk2[3], chunk2[4], chunk2[5], chunk2[6]);
    return key;
}
//  Rand Function
//  (rand() % (upper - lower +1)) + lower;

int main(int argc, char *argv[]) {
    srand(time(NULL));
    if (argc == 1) {
        printf("Too few arguements. Try --help\n");
        return 1;
    }
    if (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0) {
        usage();
    }
    else if (strcmp(argv[1], "-o") == 0 || strcmp(argv[1], "--oem") == 0) {
        if (argc != 3) {
            printf("Too few arguements. Try --help\n");
            return 1;
        }
        for (int i = 0; i < atoi(argv[2]); i++) { 
            printf("%s\n", oem());
        }
    }
    else if (strcmp(argv[1], "-c") == 0 || strcmp(argv[1], "--cd") == 0) {
        if (argc != 3) {
            printf("Too few arguements. Try --help\n");
            return 1;
        }
        for (int i = 0; i < atoi(argv[2]); i++) {
            printf("%s\n", cd());
        }
    }
    else {
        printf("Arguments not recognized. Try --help");
        return 1;
    }

    return 0;
}
