COMPILER = gcc
FLAGS = -Werror -Wall -Wextra -pedantic-errors -std=c99

TARGET = win95-key-generator
INPUT = main.c

all:
	$(COMPILER) $(FLAGS) -o $(TARGET) $(INPUT)
	
clean:
	rm -rf $(TARGET)
	$(COMPILER) $(FLAGS) -o $(TARGET) $(INPUT)

install:
	$(COMPILER) $(FLAGS) -o $(TARGET) $(INPUT)
	cp $(TARGET) /usr/bin
