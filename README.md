# win95-key-generator
A Windows 95 CD Key and OEM key generator. 

CD Keys are keys used to activate Office '95 and Consumer versions of windows 95. 

OEM Keys are keys used to activate OEM versions of Windows 95.

# Compiling:

### Linux

1. Clone the repo using ```git clone https://gitlab.com/RealStoneSword/win95-key-generator```

2. CD into the folder using ```cd win95-key-generator```

3. Compile using ```make```

4. Run from command line using ```./win95-key-generator --help```

5. Optionally, install by running ```make install```

